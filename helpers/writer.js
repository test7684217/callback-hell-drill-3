const path = require("path");
const fs = require("fs");

function writer(fileName, data, callback) {
  try {
    
    fs.mkdir("./output", { recursive: true }, (err) => {
      if (err) {
        return callback(err, "Error in creating directory");
      }
    });

    const filePath = path.join("./output",fileName);

    fs.writeFile(filePath, JSON.stringify(data), (err) => {
      try {
        callback(null, `Data stored in ${fileName} successfully`);
      } catch (error) {
        callback(error, `Callback error while writing`);
      }
    });
  } catch (error) {
    callback(error, "Error storing data in file");
  }
}

module.exports = writer;
