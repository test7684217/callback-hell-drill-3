function getAllPowerPuffData(data, callback) {
  try {
    let result = data["Powerpuff Brigade"];
    callback(null, result);
  } catch (error) {
    callback(error, null);
  }
}

module.exports = getAllPowerPuffData;
