
function removeEntryWithId(data, id, callback) {
    try {
      let result = data.filter((item) => {
        if (item.id !== id) {
          return item;
        }
      });
  
      callback(null, result);
    } catch (error) {
      callback(error, null);
    }
  }


module.exports = removeEntryWithId