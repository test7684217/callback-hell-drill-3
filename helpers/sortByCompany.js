function sortByCompany(data, callback) {
  try {
    data.sort((item1, item2) => {
      if (item1.company === item2.company) {
        return item1.id - item2.id;
      }
      return item1.company.localeCompare(item2.company);
    });

    callback(null, data);
  } catch (error) {
    callback(error, null);
  }
}

module.exports = sortByCompany;
