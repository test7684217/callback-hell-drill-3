function groupData(data, callback) {
  try {
    let groupedData = data.reduce((acc, currItem) => {
      acc[currItem.company] = [];

      return acc;
    }, {});

    data.forEach((item) => {
      let array = groupedData[item.company];
      array.push(item);
      groupedData[item.company] = array;
    });

    callback(null, groupedData);
  } catch (error) {
    callback(error, null);
  }
}

module.exports = groupData;
