function addBirthdayToEvenId(data, callback) {
  try {
    data.forEach((item) => {
      if (item.id % 2 == 0) {
        item.birthday = Date.now();
      }
    });

    callback(null, data);
  } catch (error) {
    callback(error, null);
  }
}

module.exports = addBirthdayToEvenId;
