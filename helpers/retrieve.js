function retrieve(data, idArray, callback) {
    try {
      let result = data.filter((item) => {
        return idArray.includes(item.id);
      });
  
      callback(null, result);
    } catch (error) {
      callback(error, null);
    }
  }
  
module.exports = retrieve;