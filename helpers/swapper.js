function swapper(data, idToSwap, idToBeSwappedWith, callback) {
  try {
    let index1 = -1;
    let index2 = -1;

    data.forEach((item, index) => {
      if (item.id == idToSwap) {
        index1 = index;
      } else if (item.id == idToBeSwappedWith) {
        index2 = index;
      }
    });

    let temp = data[index1];
    data[index1] = data[index2];
    data[index2] = temp;

    callback(null, data);
  } catch (error) {
    callback(error, null);
  }
}


module.exports = swapper;