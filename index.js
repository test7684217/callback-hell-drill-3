const retrieve = require("./helpers/retrieve");
const writer = require("./helpers/writer");
const groupData = require("./helpers/groupData");
const getAllPowerPuffData = require("./helpers/getAllPowerPuffData");
const removeEntryWithId = require("./helpers/removeEntryWithId");
const sortByCompany = require("./helpers/sortByCompany");
const swapper = require("./helpers/swapper");
const addBirthdayToEvenId = require("./helpers/addBirthdayToEvenId");

const employeeData = require("./1-employees-callbacks.cjs");

writer("../data.json",employeeData,(error,message)=>{
    if(error){
        return console.log(message);
    }else{

        const data = require("./data.json")
        retrieve(data.employees, [2, 13, 23], (error, result) => {
        if (error) {
            console.log(error);
            return;
        } else {
            console.log(result);
            writer("retrievedData.json", result, (error, message) => {
            if (error) {
                console.log(message);
                return;
            } else {
                groupData(data.employees, (error, groupedDataByCompany) => {
                if (error) {
                    console.log(error);
                    return;
                } else {
                    console.log(groupedDataByCompany);
                    writer(
                    "groupedDataByCompany.json",
                    groupedDataByCompany,
                    (error, message) => {
                        if (error) {
                        return console.log(message);
                        }

                        getAllPowerPuffData(groupedDataByCompany,(error, powerPuffBrigadeData) => {
                            if (error) {
                            return console.log(error);
                            } else {
                            console.log(powerPuffBrigadeData);

                            writer("powerPuffBrigadeData.json",powerPuffBrigadeData,(error, message) => {
                                if (error) {
                                    console.log(message);
                                } else {
                                    //
                                    removeEntryWithId(data.employees,2,(error,removedEntryWithIdData)=>{
                                        if (error) {
                                            return console.log(error);
                                        }

                                        let modifiedData = {
                                            "employees" : removedEntryWithIdData
                                        }

                                        writer("removeEntryWithId.json",(modifiedData),(error,message)=>{

                                            if(error){
                                                return console.log(message);
                                            }else{

                                                sortByCompany(data.employees,(error,sortedData)=>{

                                                    if(error){
                                                        return console.log(error);
                                                    }
                                                    else{
                                                        console.log(sortedData);

                                                        writer("sortByCompany.json", sortedData,(error,message)=>{

                                                            if(error){
                                                                return console.log(message);
                                                            }
                                                            else{
                                                                swapper(data.employees,93,92,(error,result)=>{

                                                                    if(error){
                                                                    return console.log(error)
                                                                    }else{

                                                                        writer("swappedData.json", sortedData,(error,message)=>{

                                                                            if(error){
                                                                                return console.log(message);
                                                                            }
                                                                            else{
                                                                                addBirthdayToEvenId(data.employees,(error,result)=>{
                                                                                    
                                                                                    if(error){
                                                                                        return console.log(error);
                                                                                    }
                                                                                    else{
                                                                                        writer("addBirthdayToEvenId.json", sortedData,(error,message)=>{

                                                                                            if(error){
                                                                                                return console.log(message);
                                                                                            }
                                                                                            else{

                                                                                                console.log("Completed !")
                                                                                            }
                                                                                        })

                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })

                                                            }
                                                        })
                                                    }
                                                })

                                            }
                                        })
                                    })
                                }
                                }
                            );
                            }
                        }
                        );
                    }
                    );
                }
                });
            }
            });
        }
        });
    }
})